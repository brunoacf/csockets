/*
 * *****************************************************************
 * 
 * This header file is part of a wrapper for the socket library. It allows
 * a user to program sockets in an Object Oriented way.
 *
 * *****************************************************************
 *
 * Copyright (C) 2001  Bruno A. Costa (bruno@codata.com.br)
 * Released under GNU Public License v2.0 (see included COPYING file)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *                                                     
 * *****************************************************************
 */


#include "CSocket.hpp"

#ifndef _C_SOCK_STREAM_
#define _CSOCK_STREAM_

namespace bcf
{

////////////////////////////////////////////////////////////////
// Class CSockStream
////////////////////////////////////////////////////////////////
class CSockStream : public CSocket
{
	protected:
		int iConnFd;
		int iListenQw;
	public:
		CSockStream      ( );
		CSockStream      ( int portno, int listenqw );
		CSockStream      ( char* hostaddr, int portno, int listenqw );
		~CSockStream     ( );
		void Bind        ( int portno );
		void Bind        ( char* hostaddr, int portno );
		void Listen      ( int );
		void Accept      ( );
		void Connect     ( const char* , int );
		std::string Read ( );
		void Write       ( std::string );
		//virtual void Write ( const char * msg );
		int getConnId    ( );
		void Close       ( );
};

////////////////////////////////////////////////////////////////
// Class CSockStream : Constructor
// Only create a socket
////////////////////////////////////////////////////////////////
CSockStream::CSockStream ( )
{
	iFamily   = PF_INET;
	iType     = SOCK_STREAM;
	iProtocol = 0;
	iSocketFd = 0;
	iConnFd   = 0;

	if ( (iSocketFd = socket( iFamily, iType, iProtocol ) ) < 0) {
		throw CSocketException ();
	}
}


////////////////////////////////////////////////////////////////
// Class CSockStream : Constructor
// Create a Server Socket (call Socket, Bind, Listen and Accept)
////////////////////////////////////////////////////////////////
CSockStream::CSockStream ( int portno, int listen_qw = 5 )
{
	iFamily   = PF_INET;
	iType     = SOCK_STREAM;
	iProtocol = 0;
	iSocketFd = 0;
	iConnFd   = 0;

	if ( (iSocketFd = socket( iFamily, iType, iProtocol ) ) < 0) {
		throw CSocketException ();
	}

	Bind   ( portno );
	Listen ( listen_qw );
}


////////////////////////////////////////////////////////////////
// Class CSockStream : Constructor
// Create a Server Socket (call Socket, Bind, Listen and Accept)
////////////////////////////////////////////////////////////////
CSockStream::CSockStream ( char* hostaddr, int portno, int listen_qw = 5 )
{
	iFamily   = PF_INET;
	iType     = SOCK_STREAM;
	iProtocol = 0;
	iSocketFd = 0;
	iConnFd   = 0;

	if ( (iSocketFd = socket( iFamily, iType, iProtocol ) ) < 0) {
		throw CSocketException ( );
	}

	Bind   ( hostaddr, portno );
	Listen ( listen_qw );
}


////////////////////////////////////////////////////////////////
// Class CSockStream : Destructor
////////////////////////////////////////////////////////////////
CSockStream::~CSockStream( )
{
	if ( iConnFd > 0 ) {
		close ( iConnFd );
	}
	
	if ( iSocketFd > 0 ) {
		close ( iSocketFd );
	}
}

////////////////////////////////////////////////////////////////
// Class CSocket : Method Bind
// Bind a name to a socket
////////////////////////////////////////////////////////////////
void CSockStream::Bind ( int portno )
{
	iPortNo = portno;

	memset ( (char *) &tpSockAddr, 0, sizeof(tpSockAddr) );

	tpSockAddr.sin_family      = iFamily;
	tpSockAddr.sin_addr.s_addr = htonl( INADDR_ANY );
	tpSockAddr.sin_port        = htons( iPortNo );

	if ( bind (iSocketFd, (struct sockaddr *) &tpSockAddr, sizeof(tpSockAddr) ) < 0)
		throw CBindSocketException ( );
}


////////////////////////////////////////////////////////////////
// Class CSocket : Method Bind
// Bind a name to a socket
////////////////////////////////////////////////////////////////
void CSockStream::Bind ( char* hostaddr, int portno )
{
	iPortNo = portno;

	memset ( (char *) &tpSockAddr, 0, sizeof(tpSockAddr) );

	tpSockAddr.sin_family = iFamily;

	if ( inet_pton (iFamily, hostaddr, &tpSockAddr.sin_addr) <= 0)
		throw CBindSocketException ( );
	
	tpSockAddr.sin_port = htons( iPortNo );

	if ( bind (iSocketFd, (struct sockaddr *) &tpSockAddr, sizeof(tpSockAddr) ) < 0)
		throw CBindSocketException ();
}

////////////////////////////////////////////////////////////////
// Class CSockStream : Method Listen
// Enables the socket to listen to connections 
////////////////////////////////////////////////////////////////
void CSockStream::Listen( int listen_qw )
{
	iListenQw = listen_qw;

	if ( (listen ( iSocketFd, iListenQw )) < 0)
		throw CListenSocketException ( ) ;

}
////////////////////////////////////////////////////////////////
// Class CSockStream : Method Accept
// Accept the connections
////////////////////////////////////////////////////////////////
void CSockStream::Accept ( )
{
	if ( (iConnFd = accept( iSocketFd, (struct sockaddr *) NULL, NULL) ) < 0 )
		throw CAcceptSocketException ( ) ;

}

////////////////////////////////////////////////////////////////
// Class CSockStream : Method Connect
// Connect to a TCP Socket
///////////////////////////////////////////////////////////////
void CSockStream::Connect ( const char* hostaddr, int portno )
{
	memset ( (char *) &tpSockAddr, 0, sizeof(tpSockAddr) );

	// For Compatibility reasons we use iConnFd instead of iSocketFd
	iConnFd = iSocketFd;
	
	tpSockAddr.sin_family = iFamily;
	tpSockAddr.sin_port   = htons ( portno );
	if ( inet_pton (PF_INET, hostaddr, &tpSockAddr.sin_addr) <= 0 )
		throw CConnectSocketException ( );

	if (connect(iConnFd,(struct sockaddr *) &tpSockAddr, sizeof(tpSockAddr)) < 0 ) {
		throw CConnectSocketException ( );
	}
}

		
////////////////////////////////////////////////////////////////
// Class CSockStream : Method Read
// Read data from a remote socket
////////////////////////////////////////////////////////////////
std::string CSockStream::Read ( )
{
	std::string msg;
	if ( iConnFd <= 0 )
		throw CNoConnectionException ( );
	
	memset ( sSockBuffer, 0, sizeof(sSockBuffer) );
	
	if ( ( recv (iConnFd, sSockBuffer, SOCK_BUFFER, 0 )) < 1 )
		throw CReadSocketException ( );

	msg = sSockBuffer;
	return ( msg );
}


////////////////////////////////////////////////////////////////
// Class CSockStream : Method Write
// Receives a string object and sends a 
// C-style string to the remote socket.
////////////////////////////////////////////////////////////////
void CSockStream::Write ( std::string msg )
{
	if ( iConnFd <= 0 )
		throw CNoConnectionException ( );

	//std::cout << msg;
	//std::cout << "TESTE\n" << std::flush;
	//msg.append ("\n");
	
	if ( send ( iConnFd, msg.c_str(), strlen (msg.c_str()), 0 ) < 1 )
		throw CWriteSocketException ( );
}


////////////////////////////////////////////////////////////////
// Class CSockStream
// Member Function: Receives a c-style string and sends it
//                  to the remote socket.
////////////////////////////////////////////////////////////////
//void CSockStream :: Write ( const char * msg )
//{
//	if ( iConnFd <= 0 )
//		throw CNoConnectionException ( );
//	
//	if ( send ( iConnFd, msg, strlen (msg), 0 ) < 0 )
//		throw CWriteSocketException ( );
//}

////////////////////////////////////////////////////////////////
// Class CSockStream : getConnId
// Returns the Identifier (File Descriptor)
// of the actual connection. 
////////////////////////////////////////////////////////////////
int CSockStream::getConnId ( )
{
	return iConnFd;
}

////////////////////////////////////////////////////////////////
// Class CServerSocket: Method Close
// Close the sockets.
////////////////////////////////////////////////////////////////
void CSockStream :: Close ( )
{
	if ( iConnFd > 0 )
		close ( iConnFd );

	//if ( iSocketFd > 0 )
	//	close ( iSocketFd );
}


} // namespace bcf

#endif // _C_SOCK_STREAM_
