#include <iostream>
#include "CSockStream.hpp"

int main (int argc, char ** argv)
{
   try {
      bcf::CSockStream ServEco ( 25000, 5 ); //chama socket, bind e listen
      while (true) {
         ServEco.Accept ( );
         ServEco.Write( ServEco.Read ( ) );
         ServEco.Close ( );
       }
   }
   catch ( bcf::CSocketException& se) {
      std::cout << se.what ( ) << std::endl;
   }
   catch ( ... ) {
      std::cout << "Unknown exception..." << std::endl;
   }
}  
