/*
 * *****************************************************************
 * 
 * This header file is part of a wrapper for the socket library. It allows
 * a user to program sockets in an Object Oriented way.
 *
 * *****************************************************************
 *
 * Copyright (C) 2001  Bruno A. Costa (bruno@codata.com.br)
 * Released under GNU Public License v2.0 (see included COPYING file)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *                                                     
 * *****************************************************************
 */


#include "CSocket.hpp"

#ifndef _C_SOCK_DGRAM_
#define _C_SOCK_DGRAM_

namespace bcf
{

////////////////////////////////////////////////////////////////
// Class CSockDgram
////////////////////////////////////////////////////////////////
class CSockDgram : public CSocket
{
	protected:
		struct sockaddr_in tpRemoteAddr;
		socklen_t sockLen;
	public:
		CSockDgram ( );
		void Bind  ( int portno );
		void Bind  ( char* hostaddr, int portno );
		std::string Read ( );
		void Write ( std::string );
		void Write ( std::string, std::string to_host, int to_port );
};


////////////////////////////////////////////////////////////////
// Method: Constructor
////////////////////////////////////////////////////////////////
CSockDgram::CSockDgram ( )
{
	iFamily   = AF_INET;
	iType     = SOCK_DGRAM;
	iProtocol = 0;
	iSocketFd = 0;

	if ( (iSocketFd = socket (iFamily, iType, iProtocol)) < 0 )
		throw CSocketException ( );
	
	memset ( (char *) &tpSockAddr, 0, sizeof(tpSockAddr) );
}

////////////////////////////////////////////////////////////////
// Class CSockDgram : Method Bind
// Bind a name to a socket
////////////////////////////////////////////////////////////////
void CSockDgram::Bind ( int portno )
{
	iPortNo = portno;

	memset ( (char *) &tpSockAddr, 0, sizeof(tpSockAddr) );

	tpSockAddr.sin_family      = iFamily;
	tpSockAddr.sin_addr.s_addr = htonl( INADDR_ANY );
	tpSockAddr.sin_port        = htons( iPortNo );

	if ( bind (iSocketFd, (struct sockaddr *) &tpSockAddr, sizeof(tpSockAddr) ) < 0)
		throw CBindSocketException ( );
}


////////////////////////////////////////////////////////////////
// Class CSockDgram : Method Bind
// Bind a name to a socket
////////////////////////////////////////////////////////////////
void CSockDgram::Bind ( char* hostaddr, int portno )
{
	iPortNo = portno;

	memset ( (char *) &tpSockAddr, 0, sizeof(tpSockAddr) );

	tpSockAddr.sin_family = iFamily;
	tpSockAddr.sin_port = htons( iPortNo );
	if ( inet_pton (iFamily, hostaddr, &tpSockAddr.sin_addr) <= 0)
		throw CBindSocketException ( );
	
	if ( bind (iSocketFd, (struct sockaddr *) &tpSockAddr, sizeof(tpSockAddr) ) < 0)
		throw CBindSocketException ();
}

////////////////////////////////////////////////////////////////
// Method: Read
// Read data from remote socket
////////////////////////////////////////////////////////////////
std::string CSockDgram::Read ( )
{
	std::string msg;
	sockLen = sizeof (tpRemoteAddr);

	memset ( sSockBuffer, 0, sizeof(sSockBuffer) );
	memset ( (char *) &tpRemoteAddr, 0, sizeof(tpRemoteAddr) );
	
	//if ((recvfrom(iSocketFd, sSockBuffer, SOCK_BUFFER, 0, (struct sockaddr *) &tpRemoteAddr, (socklen_t *) sizeof(tpRemoteAddr))) < 0)
	if ((recvfrom(iSocketFd, sSockBuffer, SOCK_BUFFER, 0, (struct sockaddr *) &tpRemoteAddr, &sockLen)) < 0)
		throw CReadSocketException ( );

	msg = sSockBuffer;
	return ( msg );
}

////////////////////////////////////////////////////////////////
// Method: Write
// Receives a c-style string and sends it to the remote socket.
////////////////////////////////////////////////////////////////
void CSockDgram::Write ( std::string msg )
{
	sockLen = sizeof (tpRemoteAddr);

	if ( sendto (iSocketFd, msg.c_str(), strlen(msg.c_str()), 0, (struct sockaddr *) &tpRemoteAddr, sockLen) < 0)
		throw CWriteSocketException ( );
}

////////////////////////////////////////////////////////////////
// Method: Write
// Receives a c-style string and sends it to the remote socket.
////////////////////////////////////////////////////////////////
void CSockDgram::Write ( std::string msg, std::string to_host, int to_port )
{
	sockLen = sizeof (tpRemoteAddr);
	memset ( (char *) &tpRemoteAddr, 0, sizeof(tpRemoteAddr) );
	tpRemoteAddr.sin_family = iFamily;
	tpRemoteAddr.sin_port   = htons (to_port);
	if ( inet_pton ( iFamily, to_host.c_str(), &tpRemoteAddr.sin_addr ) <= 0 )
		throw CWriteSocketException ( );

	if ( sendto (iSocketFd, msg.c_str(), strlen(msg.c_str()), 0, (struct sockaddr *) &tpRemoteAddr, sockLen) < 0)
		throw CWriteSocketException ( );
}


} //namespace bcf

#endif // _C_SOCK_DGRAM
