/*
 * *****************************************************************
 * 
 * This header file is part of a wrapper for the socket library. It allows
 * a user to program sockets in an Object Oriented way.
 *
 * *****************************************************************
 *
 * Copyright (C) 2001  Bruno A. Costa (bruno@codata.com.br)
 * Released under GNU Public License v2.0 (see included COPYING file)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *                                                     
 * *****************************************************************
 */

#ifndef __C_SOCKET__
#define __C_SOCKET__ 1

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include <string.h>
#include <string>
#include <unistd.h>

#include "CException.hpp"

namespace bcf {

class CSocket
{
	protected:
		const static unsigned short int SOCK_BUFFER = 4096;
		int    iFamily;
		int    iType;
		int    iProtocol;
		int    iSocketFd;
		int    iPortNo;
		struct sockaddr_in tpSockAddr;
		char   sSockBuffer [SOCK_BUFFER];
	
	public:
		virtual ~CSocket         ( ) { };
		virtual void Bind        ( int portno ) = 0;
		virtual void Bind        ( char* hostaddr, int portno ) = 0;
};

} // namespace bcf

# endif // __C_SOCKET__
