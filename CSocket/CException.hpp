//////////////////////////////////////////////////
// Socket Exceptions
//////////////////////////////////////////////////
#ifndef __SOCKET_EXCEPT
#define __SOCKET_EXCEPT 1

#include <stdexcept>
#include <string>
#include <cstring>
#include <errno.h>
//#include <stdio.h>

namespace bcf {

class CSocketException : public std::exception
{
	protected:
		std::string sError;		
	public:
		CSocketException ( );
		virtual ~CSocketException ( ) throw ( );
		virtual const char* what( ) throw ( );
};

class CBindSocketException : public CSocketException
{
	public:
		virtual const char* what ( ) throw ( );
};

class CListenSocketException : public CSocketException
{
	public:
		virtual const char* what ( ) throw ( );
};

class CAcceptSocketException : public CSocketException
{
	public:
		virtual const char* what ( ) throw ( );
};

class CConnectSocketException : public CSocketException
{
 	public:
		virtual const char* what ( ) throw ( );
};

class CReadSocketException : public CSocketException
{ 
	public:
		virtual const char* what ( ) throw ( );
};

class CWriteSocketException : public CSocketException
{ 
	public:
		virtual const char* what ( ) throw ( );
};

class CNoConnectionException : public CSocketException
{ 
	public:
		virtual const char* what ( ) throw ( );
};


//////////////////////////////////////////////////////////
// Definitions of methods
//////////////////////////////////////////////////////////
CSocketException::CSocketException ( )
{ }

CSocketException::~CSocketException ( ) throw ( )
{ }


const char* CSocketException::what ( ) throw ( )
{
	sError = "SocketException: ";
	//sError.append ( sys_errlist[errno] );
	sError.append ( std::strerror(errno) );
	return sError.c_str();
}


const char* CBindSocketException::what ( ) throw ( )
{
	sError = "BindSocketException: ";
	//sError.append ( sys_errlist[errno] );
	sError.append ( std::strerror(errno) );
	return sError.c_str();
}

const char* CListenSocketException::what ( ) throw ( )
{
	sError = "ListenSocketException: ";
	//sError.append (sys_errlist[errno]);
	sError.append ( std::strerror(errno) );
	return sError.c_str();
};

const char* CAcceptSocketException::what ( ) throw ( )
{
	sError = "AcceptSocketException: ";
	//sError.append (sys_errlist[errno]);
	sError.append ( std::strerror(errno) );
	return sError.c_str();
};

const char* CConnectSocketException::what ( ) throw ( )
{
	sError = "ConnectSocketException: ";
	//sError.append (sys_errlist[errno]);
	sError.append ( std::strerror(errno) );
	return sError.c_str();
};

const char* CReadSocketException::what ( ) throw ( )
{ 
	sError = "ReadSocketException: ";
	//sError.append (sys_errlist[errno]);
	sError.append ( std::strerror(errno) );
	return sError.c_str();

};

const char* CWriteSocketException::what ( ) throw ( )
{ 
	sError = "WriteSocketException: ";
	//sError.append (sys_errlist[errno]);
	sError.append ( std::strerror(errno) );
	return sError.c_str();
};

const char* CNoConnectionException::what ( ) throw ( )
{ 
	sError = "NoConnectionException: ";
	//sError.append (sys_errlist[errno]);
	sError.append ( std::strerror(errno) );
	return sError.c_str();
};

} // namespace bcf

#endif // __SOCKET_EXCEPT
