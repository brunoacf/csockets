/*
 * *****************************************************************
 * portfw - Port Forwarder
 * A single port forwarder
 * Released under GNU Public License v2.0 (see included COPYING file)
 *
 * Copyright (C) 2001  Bruno A. Costa (bruno@codata.com.br)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *                                                     
 * *****************************************************************
 */
#include <stdio.h>
#include <iostream>

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "../CSocket/CSockStream.hpp"


#define VERSION     "v.0.3.0 11/19/2002 bruno@codata.com.br"
#define BANNER      "\nportfw version " VERSION "\n" \
					"This software is released under the GNU Public License\n" \
					"Copyright (C) 2001 Bruno A. Costa <bruno@codata.com.br>\n\n" 
#define MAX_BACKLOG 10
#define BUFFER_SIZE 4096
#define MAX_CHILD   30

class portfw
{
	private:
		//const char sVersion = VERSION;
		//const char sBanner = BANNER;
		//const int  iBackLog = MAX_BACKLOG;
		//const int  iBufferSize = BUFFER_SIZE;
		//const int  iMaxChild = MAX_CHILD;
	public:
		int   iChildCount;
		int   iMaxClients;
		char* sLocalAddress;
		int   iLocalPort;
		char* sServerAddress;
		int   iServerPort;
		bool  daemonized;

		void usage ( char* progname );
		//void catch_sigchld ( int portno );
		void writemsg (char *message);
		int redirect(bcf::CSockStream, char *serv_address, int serv_portno);
		bool parse_cmd_line(int argc, char **argv);

};

void (*Signal (int signo, void (*sig_handler)(int))) (int)
{
	struct sigaction sa_new, sa_old;

	sa_new.sa_handler = sig_handler;
	sigemptyset(&sa_new.sa_mask);
	sa_new.sa_flags = 0;
	if (signo == SIGALRM) {
#ifdef SA_INTERRUPT
		sa_new.sa_flags |= SA_INTERRUPT;
#endif
	} else {
#ifdef SA_RESTART
		sa_new.sa_flags |= SA_RESTART;
#endif
	}
	if ( sigaction(signo, &sa_new, &sa_old) < 0 )
		return(SIG_ERR);
	return(sa_old.sa_handler);
}

/* Just show usage */
void portfw::usage(char *progname)
{
	std::cout << BANNER;
	std::cout << "Usage: " << progname << " [-a <local address>] -l <local port> -s <server host> -p <server port> [-F]\n\n";
}

/* Write messages/alerts/errors */
void portfw::writemsg(char *message)
{
	if (errno > 0)
		perror(message);
	else if (message)
		std::cout << "\n" <<  message << "\n";
}

/*
 * Handler for the SIG_CHLD signal
 * We use waitpid() inside a while to prevent loss of signals
 * when two or more children terminate at aproximately the same time.
 * 
 * Notice that to use waitpid() inside a loop, we need to set it non
 * blocking, with the parameter WNOHANG.
 * 
 */
void catch_sigchld(int signo)
{
	int child_count;
	pid_t pid;
	int stat;

	while ( (pid = waitpid(-1, &stat, WNOHANG)) > 0 ) {
		if (child_count > 0)
			child_count--;
		else
			printf("WARNING: SIGCHLD received when child_count < 1");
	}
}

/*
 * Redirect all arriving data to the real server.
 * There is a separate instance of this function for
 * each connection.
 *
 * Returns: 0 if OK. A negative value on errors.
 *
 */
int portfw::redirect(bcf::CSockStream fromClient, char *serv_address, int serv_portno)
{
	fd_set frwd_fds;
	std::string frwd_buffer; /* Buffer to forward data */
	int servcon, clicon;

	bcf::CSockStream toServer;
	toServer.Connect ( serv_address, serv_portno );
	std::cout << "Servidor: " << serv_address;

	servcon = toServer.getConnId ( );
	clicon = fromClient.getConnId ( );
	
	std::cout << "\nfromClient = " << fromClient.getConnId( );
	std::cout << "\ntoServer = " << toServer.getConnId( ) << std::endl;

	fflush ( stdout);

	while(true){
		FD_ZERO( &frwd_fds );
		FD_SET( servcon, &frwd_fds );
		FD_SET( clicon, &frwd_fds );
		select( FD_SETSIZE, &frwd_fds, NULL, NULL, NULL );
		
		if ( FD_ISSET(clicon, &frwd_fds) ) {
			/* Read from client and write to server... */
			//toServer.Write ( fromClient.Read( ) );
			frwd_buffer = fromClient.Read ( );
			toServer.Write ( frwd_buffer );
			std::cout << "Client > "<< frwd_buffer << std::endl;
		}
		
		if ( FD_ISSET(servcon, &frwd_fds) ) {
			/* Read from server and write to client... */
			//fromClient.Write ( toServer.Read( ) );
			frwd_buffer = toServer.Read ( );
			fromClient.Write ( frwd_buffer );
			std::cout << "Server > "<< frwd_buffer << std::endl;
		}
	}
	fromClient.Close ( );
	toServer.Close ( );

	return(0);
}

/*
 * Parse command line options and store values on
 * global  struct main_opt.
 * 
 */
bool portfw::parse_cmd_line(int argc, char **argv)
{
	int optch;
	
	/* Some default options (may be overwritten by command line options) */
	sLocalAddress  = NULL;
	iLocalPort     = 0;
	sServerAddress = NULL;
	iServerPort    = 0;
	daemonized     = true;
	iMaxClients    = MAX_CHILD;
	
	while ( (optch = getopt(argc, argv, "a:l:s:p:C:F")) != -1 ){
		switch(optch) {
			case 'a' :
				sLocalAddress = new char [sizeof(optarg) + 1];
				if (sLocalAddress != NULL) strcpy(sLocalAddress, optarg);
			case 'l' :
				iLocalPort = atoi(optarg);
				break;
			case 's' :
				sServerAddress = new char[ sizeof(optarg) + 1 ];
				if (sServerAddress != NULL) strcpy(sServerAddress, optarg);
				break;
			case 'p' :
				iServerPort = atoi(optarg);
				break;
			case 'F' :
				daemonized = false;
				break;
			case 'C' :
				iMaxClients = atoi(optarg);
				break;
		}
	}
	
	if (iLocalPort <= 0) {
		printf("\n*** A local port must be declared! ***\n");
		return(false);
	}
	if (sServerAddress == NULL) {
		printf("\n*** The server host was not defined! ***\n");
		return(false);
	}
	if (iServerPort <= 0) {
		printf("\n*** The server port was not defined! ***\n");
		return(false);
	}
	
	printf("Local host: %s\n", sLocalAddress);
	printf("Local port: %i\n", iLocalPort);
	printf("Server host: %s\n", sServerAddress);
	printf("Server port: %i\n", iServerPort);
	printf("Daemonized: %i\n", daemonized);
	printf("Clients: %i\n", iMaxClients);

	return(true);
}

/* main */
int main(int argc, char **argv)
{
	pid_t pid;
	portfw pf;
	if (argc < 4){
		pf.usage(argv[0]);
		exit(1);
	}

	pf.iChildCount = 0;
	// Parse command line options 
	if (! pf.parse_cmd_line(argc, argv)) {
		pf.usage(argv[0]);
		exit(1);
	}

	if ( pf.daemonized ) {		// Daemonize our process
		if ( (pid = fork()) != 0 )
			exit(0);						// Parent terminates. 1st child continues
		setsid( );							// 1st child becomes session leader
		if ( (pid = fork()) != 0)
			exit(0);						// 1st child terminates. 2nd child continues
		// if OK, now we're running as a daemon 
	}

	// Handler for SIGCHLD - to avoid zombie processes
	Signal(SIGCHLD, catch_sigchld);

	
	bcf::CSockStream localSocket (pf.sLocalAddress, pf.iLocalPort, 30);
	
	//  if the parent receives a SIGCHLD in a blocked accept, it forces accept
	//  to return an "Interrupted system call" error (EINTR), and the program
	//  aborts.
	//  So, we must check for a possible EINTR error returned by accept() and,
	//  in this case, force a loop in the while().
	// 
	while (true) {
		try {
			std::cout << "\nACEITANDO ...\n";
			std::cout << "Conexao " << localSocket.getConnId() << std::endl;
			localSocket.Accept ( );
		}
		catch (bcf::CAcceptSocketException se) {
			if (errno == EINTR) {
				std::cout << "\nEINTR\n";
				continue;
			}
			else {
				std::cout << se.what ( ) << std::endl;
			}
		}
		catch ( ... ) {
			std::cout << "Unknown Exception ...\n";
		}
		
		if (pf.iChildCount < pf.iMaxClients){
			std::cout << "\nBifurcando ...\n";
			if ( (pid = fork()) == 0) {
				std::cout << "\nIniciando Redirecionamento...\n";
				if (pf.redirect(localSocket, pf.sServerAddress, pf.iServerPort) < 0)
					pf.writemsg("Failed attempting to redirect data");
				localSocket.Close( );	// Child closes its connected socket
				exit(0);				// End of the child process
			}
			if (pid > 0)
				pf.iChildCount++;		// Parent increments child counter
			else
				pf.writemsg("Error forking");
		}
		localSocket.Close( );			// Parent closes his connected socket
	}
}

