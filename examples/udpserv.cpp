#include <iostream>
#include "../CSocket/CSockDgram.hpp"

int main ( )
{
	std::string msg;
	int i = 0;
	try {
		bcf::CSockDgram dgsock;
		dgsock.Bind ( "127.0.0.1", 41000 );

		while (true) {
			msg = dgsock.Read ( );
		
			std::cout << "\nMensagem " << i << ": " << msg << std::endl;

			msg = "Recebi " + msg;
			dgsock.Write ( msg );
			i++;
		}
	}
	catch (bcf::CSocketException & se) {
		std::cout << se.what ( ) << std::endl;
	}
	catch (...) {
		std::cout << "Unknown exception ...\n";
	}
}
