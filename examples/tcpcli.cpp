#include <iostream>
#include <cstdlib>
#include "../CSocket/CSockStream.hpp"

int main ( int argc, char ** argv )
{
	if ( argc != 3 ) {
		std::cout << "Usage: " << argv[0] << " <IP> <port>\n";
		return 1;
	}
	
	std::string mensagem;
	
	try {
		bcf::CSockStream sock;
		sock.Connect ( argv[1], atoi ( argv[2] ) );

		while (true) {
			std::cout << "\n> ";
			std::cin >> mensagem;
			sock.Write ( mensagem );

			mensagem = sock.Read ( );

			std::cout << "\nRecebi: " << mensagem;
		}
			sock.Close ( );
	}
	catch ( bcf::CSocketException& se )
	{
		std::cout << se.what ( ) << std::endl;
	}
	catch ( ... ) {
		std::cout << "Unknown exception ...\n";
	}
}
