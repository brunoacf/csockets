#include <iostream>
#include "../CSocket/CSockStream.hpp"

int main ( int argc, char** argv )
{
	if (argc != 4) {
		std::cout << "Usage: " << argv[0] << " <IP> <port> <Listen Queue>\n";
		return 1;
	}
	
	try {
		bcf::CSockStream * sock = new bcf::CSockStream;
		sock->Bind ( argv[1], atoi (argv[2]) );
		sock->Listen ( atoi (argv[3]) );
		while (true) {
			sock->Accept ( );
	
			std::string mensagem;
	
			mensagem = sock->Read ( );
			mensagem = "Recebi: " + mensagem;
			std::cout << mensagem;

			std::cout << "\nId.: " << sock->getConnId ( ) << std::endl;
	
			sock->Write ( mensagem );
		}
		delete (sock);
	}
	catch ( bcf::CSocketException& se) {
		std::cout << se.what ( ) << std::endl;
	}
	catch ( ... ) {
		std::cout << "Unknown exception..." << std::endl;
	}
	std :: cout << "OK\n";

	return 0;
}
