#include <iostream>
#include <cstdlib>
#include "../CSocket/CSockStream.hpp"

int main ( int argc, char ** argv )
{
	if ( argc != 3 ) {
		std::cout << "Usage: " << argv[0] << " <IP> <port>\n";
		return 1;
	}
	
	std::string mensagem;

	std::string server = argv[1];
	int port = atoi(argv[2]);

	if (port < 0 or port > 65535) {
		std::cout << "Invalid port number: " << port << std::endl;
		return 2;
	}

	std::cout << "Connecting to " << server << ":" << port << std::endl;

	
	try {
		bcf::CSockStream sock;
		sock.Connect ( server.c_str(), port );

		while (true) {
			mensagem = sock.Read ( );
			std::cout << mensagem;
			std::getline(std::cin, mensagem);
			mensagem.append("\n");
			sock.Write (mensagem);
		}
			sock.Close ( );
	}
	catch ( bcf::CSocketException& se )
	{
		std::cout << se.what ( ) << std::endl;
	}
	catch ( ... ) {
		std::cout << "Unknown exception ...\n";
	}
}
