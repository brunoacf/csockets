#include <iostream>
#include "../CSocket/CSockDgram.hpp"

int main ( )
{
	std::string msg;
	try {
		while (true) {
			std::cout << "\n>";
			//getline (msg, 30) ;
			std::cin >> msg;
			bcf::CSockDgram dgsock;
			dgsock.Write ( msg, "127.0.0.1", 41000 );
			std::cout << dgsock.Read ( ) << std::endl;
		}
	}
	catch (bcf::CSocketException & se) {
		std::cout << se.what ( ) << std::endl;
	}
	catch (...) {
		std::cout << "Unknown exception ...\n";
	}
}
